package ru.itmo.android_test_task;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import java.util.Calendar;

public class AnalogClockView extends View {

    private int height, width = 0;
    private int padding = 0;
    private int fontSize = 0;
    private int numeralSpacing = 0;
    private int hourHandSize;
    private int minuteHandSize;
    private int secondHandSize;
    private int tailSize;
    private double angle;
    private float hour;
    private float minute;
    private float second;
    private int radius = 0;
    private Paint paint;
    private boolean isInit;
    private int[] numbers = {1,2,3,4,5,6,7,8,9,10,11,12};
    private Rect rect = new Rect();



    public AnalogClockView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void initClock() {
        height = getHeight();
        width = getWidth();
        padding = numeralSpacing + 50;
        int min = Math.min(height, width);
        fontSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, (float) (min / 30),
                getResources().getDisplayMetrics());
        radius = min / 2 - padding;
        hourHandSize = radius-radius/2;
        minuteHandSize = radius-radius/3;
        secondHandSize = radius-radius/6;
        tailSize = radius/4;
        paint = new Paint();
        isInit = true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (!isInit) {
            initClock();
        }

        canvas.drawColor(Color.WHITE);
        drawCircle(canvas);
        drawCenter(canvas);
        drawNumeral(canvas);
        drawHands(canvas);
        drawDots(canvas);

        postInvalidateDelayed(500);
        invalidate();
    }

    private void setPaintAttributes(int colour, Paint.Style stroke, int strokeWidth) {
        paint.reset();
        paint.setColor(colour);
        paint.setStyle(stroke);
        paint.setStrokeWidth(strokeWidth);
        paint.setAntiAlias(true);
    }


    private void drawHands(Canvas canvas) {
        Calendar calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        //convert to 12hour format from 24 hour format
        hour = hour > 12 ? hour - 12 : hour;
        minute = calendar.get(Calendar.MINUTE);
        second = calendar.get(Calendar.SECOND);

        drawHourHand(canvas, (hour + minute / 60.0) * 5f);
        drawMinuteHand(canvas, minute);
        drawSecondsHand(canvas, second);
    }

    private void drawSecondsHand(Canvas canvas, float location) {
        paint.reset();
        setPaintAttributes(Color.BLACK, Paint.Style.STROKE,6);
        angle = Math.PI * location / 30 - Math.PI / 2;
        paint.setShadowLayer(10, 25, 25, Color.argb(125, 0, 0, 0));
        //main part
        canvas.drawLine(width/2,
                height/2,
                (float) (width/2 + Math.cos(angle) * secondHandSize),
                (float) (height/2 + Math.sin(angle) * secondHandSize),
                paint);
        //tail
        canvas.drawLine(width/2,
                height/2,
                (float) (width/2 + Math.cos(angle - Math.PI) * tailSize),
                (float) (height/2 + Math.sin(angle - Math.PI) * tailSize),
                paint);
    }

    private void drawMinuteHand(Canvas canvas, float location) {
        paint.reset();
        setPaintAttributes(Color.BLACK, Paint.Style.STROKE,12);
        angle = Math.PI * location / 30 - Math.PI / 2;
        paint.setShadowLayer(10, 25, 25, Color.argb(125, 0, 0, 0));
        //main part
        canvas.drawLine(width/2,
                height/2,
                (float) (width/2 + Math.cos(angle) * minuteHandSize),
                (float) (height/2 + Math.sin(angle) * minuteHandSize),
                paint);
        //tail
        canvas.drawLine(width/2,
                height/2,
                (float) (width/2 + Math.cos(angle - Math.PI) * tailSize),
                (float) (height/2 + Math.sin(angle - Math.PI) * tailSize),
                paint);
    }

    private void drawHourHand(Canvas canvas, double location) {
        paint.reset();
        setPaintAttributes(Color.BLACK, Paint.Style.STROKE,18);
        angle = Math.PI * location / 30 - Math.PI / 2;
        paint.setShadowLayer(10, 25, 25, Color.argb(125, 0, 0, 0));
        canvas.drawLine(width/2,
                height/2,
                (float) (width/2 + Math.cos(angle) * hourHandSize),
                (float) (height/2 + Math.sin(angle) * hourHandSize),
                paint);
        canvas.drawLine(width/2,
                height/2,
                (float) (width/2 + Math.cos(angle - Math.PI) * tailSize),
                (float) (height/2 + Math.sin(angle - Math.PI) * tailSize),
                paint);
    }

    private void drawNumeral(Canvas canvas) {
        paint.reset();
        paint.setTextSize(fontSize);

        for (int number : numbers) {
            String tmp = String.valueOf(number);
            paint.getTextBounds(tmp, 0, tmp.length(), rect);
            double angle = Math.PI / 6 * (number - 3);
            int x = (int) (width / 2 + Math.cos(angle) * (radius - radius/12) - rect.width() / 2);
            int y = (int) (height / 2 + Math.sin(angle) * (radius - radius/12) + rect.height() / 2);
            canvas.drawText(tmp, x, y, paint);
        }
    }

    private void drawDots(Canvas canvas) {
        paint.reset();
        for (int i = 0; i < 60; i++) {
            double angle = Math.PI/2 + i * Math.PI/30;
            int x = (int) (width / 2 + Math.cos(angle) * (radius + radius/28));
            int y = (int) (height / 2 + Math.sin(angle) * (radius + radius/28));
            paint.setStyle(Paint.Style.FILL);
            if (i % 5 == 0) {
               canvas.drawCircle(x, y, (float) radius/60, paint);
            } else {
                canvas.drawCircle(x, y, (float) radius/120, paint);
            }
        }
    }

    private void drawCenter(Canvas canvas) {
        paint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(width / 2, height / 2, 12, paint);
    }

    private void drawCircle(Canvas canvas) {
        paint.reset();
        setPaintAttributes(Color.BLACK, Paint.Style.STROKE, 20);
        paint.setShadowLayer(15, 15, 0, Color.argb(125, 0, 0, 0));
        canvas.drawCircle(width / 2, height / 2, radius + padding - 10, paint);
    }

}
